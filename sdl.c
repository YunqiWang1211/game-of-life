#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <SDL2/SDL.h>

#include "globalvariables.h"
#include "sdl.h"

void visual(int row, int col) {
	int length = 720;
	int width= length/row*col;
	SDL_Window *window = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	        fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
	}
    	atexit(SDL_Quit);
	
   	SDL_CreateWindowAndRenderer(width, length, 0, &window, &renderer);
    	SDL_SetWindowTitle( window, "Game of Life");
    	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    	SDL_RenderClear(renderer);
    	SDL_RenderPresent(renderer);

	alivepicture = SDL_LoadBMP("images/alive.bmp");
    	deadpicture = SDL_LoadBMP("images/dead.bmp");
	if(alivepicture==NULL||deadpicture==NULL){
		printf("The pictures is not exist\n");
		exit(0);
	}

	alive = SDL_CreateTextureFromSurface(renderer, alivepicture);
    	dead = SDL_CreateTextureFromSurface(renderer, deadpicture);

}

void render(int row, int col) {
	int square = 720/row;
	SDL_Rect rect;
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			rect.h = square;
   			rect.w = square;
    			rect.x = j * square;
   			rect.y = i * square;
			if(table[i][j] == 2) {
				SDL_RenderCopy(renderer, alive, NULL, &rect);
			}
			else {
				SDL_RenderCopy(renderer, dead, NULL, &rect);
			}
		}
	}
    	SDL_RenderPresent(renderer);
}
