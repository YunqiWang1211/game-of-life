#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#include <globalvariables.h>

int main(int argc, char **argv){
	int onefile;
	int onestep;
	int filestep;
	int row;
	int col;
	int step = 0;
	if(argc==2){
		if(atoi(argv[1]==0)){
			onefile=1;
		}
		else if(atoi(argv[1])>0){
			onestep=1;
		}
		else{
			printf("The number of steps should be a positive number.");
			return 0;
		}
	}
	if(argc==3 && atoi(argv[2])<=0){
		printf("The number of steps should be a positive number.");
			return 0;
	}
	else if(argc==3){
		filestep=1;
	}
}
