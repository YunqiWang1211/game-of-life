
# code details

EXE_DIR = .
EXE = $(EXE_DIR)/gameoflife

SRC= main.c gamefunction.c sdl.c

# generic build details

CC =     gcc
CFLAGS = -Wall -O
LIBS =   -lSDL2 -lSDL2_image

# compile to  object code

OBJ= $(SRC:.c=.o)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $< 

# build executable

$(EXE): $(OBJ)
	$(CC) $(OBJ) -o $(EXE) $(LIBS)

# clean up compilation

clean:
	rm -f $(OBJ) $(EXE)

# dependencies

main.o:  main.c globalvariables.h
game.o:  gamefunction.c globalvariables.h
sdl.o:  sdl.c globalvariables.h sdl.h
