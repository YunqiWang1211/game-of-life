#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <SDL2/SDL.h>

#include "globalvariables.h"

int init(FILE *file, int row, int col){
	if(row<=0||col<=0){
		printf("The row or col should be a positive number.\n");
		return -1;
	}
	if(row>100 || col>100){
		printf("The size of table is so big, please reduce the row or col smaller than 100.\n");
		return -1;
	}
	
	table = (int **)malloc(sizeof(int *) * row);
	for (int i = 0; i < row; i++) {
		table[i] = (int *)malloc(sizeof(int) * col);
	}
	
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			fscanf(file,"%d",&table[i][j]);
		}
	}
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			if(table[i][j]!=1&&table[i][j]!=2)
			{
				printf("The cells of table has unknown status\n");
				return -1;
			}
		}
	}
	return 0;
}